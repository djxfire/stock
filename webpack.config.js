'use strict'

var webpack = require('webpack')
var path = require('path')

module.exports = {
    entry: __dirname + '/index.js',
    //entry: __dirname + '/src/main.js',
    output: {
        path: __dirname + '/dist/',
        libraryTarget: "umd",  //一般都会选择umd
        filename: 'index.js'
    },
    //devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                },
                exclude: /node_modules/
            }
        ]
    }
}