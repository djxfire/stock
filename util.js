var URL = {}
URL.history_data_day = "http://web.ifzq.gtimg.cn/appstock/app/fqkline/get?_var=kline_dayqfq&param={$code},day,,,{$count},qfq&r=0.05682517060823766"
URL.history_data_week = "http://web.ifzq.gtimg.cn/appstock/app/fqkline/get?_var=kline_weekqfq&param={$code},week,,,{$count},qfq&r=0.8124623690151882"
URL.history_data_month = "http://web.ifzq.gtimg.cn/appstock/app/fqkline/get?_var=kline_monthqfq&param={$code},month,,,{$count},qfq&r=0.23820520654929855"
URL.history_data_minite_5 = "http://ifzq.gtimg.cn/appstock/app/kline/mkline?param={$code},m5,,{$count}&_var=m5_today&r=0.43568352127276144"
URL.history_data_minite_30 = "http://ifzq.gtimg.cn/appstock/app/kline/mkline?param={$code},m30,,{$count}&_var=m30_today&r=0.8314789594389622"
URL.history_data_minite_60 = "http://ifzq.gtimg.cn/appstock/app/kline/mkline?param={$code},m60,,{$count}&_var=m60_today&r=0.30824496038290006"
URL.history_data_minite = "http://web.ifzq.gtimg.cn/appstock/app/minute/query?_var=min_data_{$code}&code={$code}&r=0.7403477579658129"
URL.stock_list = "http://quote.eastmoney.com/stocklist.html"
URL.fund_view = "http://web.ifzq.gtimg.cn/fund/zcjj/zcjj/allzc?colum=3&order=desc&page={$page}&pagesize=50&bgrq={$date}&_var=v_jjcg"//基金重仓股
URL.fund_view_inc = "http://web.ifzq.gtimg.cn/fund/zcjj/zcjj/inczc?colum=3&order=desc&page={$page}&pagesize=50&bgrq={$date}&_var=v_jjcg"//基金增仓股
URL.fund_view_desc = "http://web.ifzq.gtimg.cn/fund/zcjj/zcjj/deczc?colum=3&order=desc&page={$page}&pagesize=50&bgrq={$date}&_var=v_jjcg"//基金减仓股
URL.fund_view_new = "http://web.ifzq.gtimg.cn/fund/zcjj/zcjj/newzc?colum=3&order=desc&page={$page}&pagesize=50&bgrq={$date}&_var=v_jjcg"//基金新建仓股
URL.fund_view_clear = "http://web.ifzq.gtimg.cn/fund/zcjj/zcjj/clearzc?colum=3&order=desc&page={$page}&pagesize=50&bgrq={$date}&_var=v_jjcg" //基金清仓股
URL.ths_hy = "http://q.10jqka.com.cn/thshy/";
URL.ths_hy_stock = "http://q.10jqka.com.cn/thshy/detail/field/199112/order/desc/page/{$page}/ajax/1/code/{$code}"
URL.ths_hy_minite = "http://d.10jqka.com.cn/v4/time/bk_{$code}/last.js"
URL.ths_hy_history_day = "http://d.10jqka.com.cn/v4/line/bk_{$code}/01/last.js"
URL.ths_hy_history_week = "http://d.10jqka.com.cn/v4/line/bk_{$code}/11/last.js"
URL.ths_hy_history_month = "http://d.10jqka.com.cn/v4/line/bk_{$code}/21/last.js"
URL.finance_abstract = "http://soft-f9.eastmoney.com/soft/gp13.php?code={$code}{$type}"
URL.money_flow = "http://ff.eastmoney.com/EM_CapitalFlowInterface/api/js?type=hff&rtntype=2&check=TMLBMSPROCR&acces_token=1942f5da9b46b069953c873404aad4b5&id={$code}{$type}&_=1526024473514"
URL.ggcg = "http://data.eastmoney.com/DataCenter_V3/gdzjc.ashx?pagesize=50&page={$page}&sortRule=-1&sortType=BDJZ&tabid=all&code={$code}&name=&rt=50867632"

var FINANCE_MAP = {
    'MGSY_JB': 2,//每股收益-基本（元）
    'MGSY_XS': 3,//每股收益-稀释（元）
    'MGJZC' : 4,//每股净资产（元）
    'MGXJLLJE': 5,//每股经营活动产生的现金流量净额（元）
    'JZCSYL' :6, //净资产收益率（%）
    'ZZCJLL' :7,//总资产净利率(%)
    'XSMLL' :8, //销售毛利率（%）
    'XSJLL' :9, //销售净利率（%）
    'ZCFZL' :10,//资产负债率（%）
    'YYLRTBZZL': 11,//营业利润同比增长率（%）
    'MGSJLRTBZZL': 12,//归属母公司股东的净利润同比增长率(%)
    'YYZSR': 14,//营业总收入
    'YYZCB': 15, //营业总成本
    'YYSR' : 16,//营业收入
    'YYLR' : 17,//营业利润
    'LRZE' : 18,//利润总额
    'JLR'  : 19,//净利润
    'MGSJLR': 20,//归属母公司股东的净利润
    'ZCZJ'  : 22,//资产总计
    'FZHJ'  :23,//负债合计
    'GDQY'  :24,//股东权益
    'MGSGDQY' :25,//归属母公司股东的权益
    'JYHDXJLL' :27,//经营活动产生的现金流量
    'TZHDXJLL' :28,//投资活动产生的现金流量
    'CZHDXJLL' :29,//筹资活动产生的现金流量
    'XJDJWZJ'  :30,//现金及现金等价物净增加
}

function empty(str) {
    return str == undefined || str == 0 || str == ''
}

function date_format (date, fmt){
    var o = {
        'M+': date.getMonth() + 1,
        'd+': date.getDate(),
        'h+': date.getHours(),
        'm+': date.getMinutes(),
        's+': date.getSeconds(),
        'q+': Math.floor((date.getMonth() + 3)/3),
        'S' : date.getMilliseconds()
    };
    if(/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
    }
    for (var k in o) {
        if(new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1)?(o[k]):(("00"+ o[k]).substr((""+ o[k]).length)))
        }
    }
    return fmt
}

exports.URL = URL
exports.empty = empty
exports.date_format = date_format
exports._FINANCE_MAP_ = FINANCE_MAP